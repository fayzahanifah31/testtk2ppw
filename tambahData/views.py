from django.shortcuts import render, redirect
from main.models import Pulau, Provinsi, Info, WebsitePemda, TelponPemda, EmailPemda, VerifiedInfo, NonVerifiedInfo
from .forms import tambahDataForm
from datetime import datetime

from .forms import tambahDataForm

from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from django.contrib.auth.decorators import login_required
# Create your views here.

@login_required(login_url='autentikasi:login')
def formTambah(request):
    infos = NonVerifiedInfo.objects.all()

    form = tambahDataForm(request.POST or None)


    if request.method == "POST":
        if form.is_valid():

            tanggal = datetime.today()
            nama_provinsi=request.POST['nama_provinsi']
            nama_pulau=request.POST['nama_pulau']
            info_protokol=request.POST['info_protokol_kesehatan']
            alamat_web=request.POST['Alamat_Web']
            email_pemda=request.POST['Email_Pemda']
            nomor_telpon=request.POST['Nomor_Telpon']
            email_user=request.POST['Email_User']
            
            pulau = Pulau.objects.get(nama = nama_pulau)
            provinsi = Provinsi.objects.create(nama = nama_provinsi, nama_pulau = pulau)
            info = Info.objects.create(tanggal = tanggal, nama_provinsi = provinsi, protokol_kesehatan = info_protokol)
            website = WebsitePemda.objects.create(info_id = info, alamat = alamat_web)
            no_telp = TelponPemda.objects.create(info_id = info, nomor_telpon = nomor_telpon)
            pemda = EmailPemda.objects.create(info_id = info, email = email_pemda)
            non_verified = NonVerifiedInfo.objects.create(info_id = info, email = email_user)

            form = tambahDataForm()
            return redirect('tambahData:formTambah')
            
        
    context = {
        'form' : form,
        'infos' : infos,
    }
    return render(request, 'tambahData/formTambah.html', context)