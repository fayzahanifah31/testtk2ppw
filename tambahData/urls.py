from django.urls import path

from . import views

app_name = 'tambahData'

urlpatterns = [
    path('', views.formTambah, name='formTambah'),
]
