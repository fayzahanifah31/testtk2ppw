from django.test import TestCase, Client
from django.urls import resolve
from django.apps import apps
from .apps import AutentikasiConfig
from django.contrib.auth.models import User

from . import views

class UnitTestForAutentikasi(TestCase):

    def test_response_page(self):
        response = Client().get('/autentikasi/')
        self.assertEqual(response.status_code, 200)

    def test_template_used_1(self):
        response = Client().get('/autentikasi/login/')
        self.assertTemplateUsed(response, 'autentikasi/login.html')

    def test_template_used_2(self):
        response = Client().get('/autentikasi/register/')
        self.assertTemplateUsed(response, 'autentikasi/register.html')

    def test_func_login(self):
        found = resolve('/autentikasi/login/')
        self.assertEqual(found.func, views.loginPage)

    def test_func_register(self):
        found = resolve('/autentikasi/register/')
        self.assertEqual(found.func, views.registerPage)

    def test_func_logout(self):
        found = resolve('/autentikasi/logout/')
        self.assertEqual(found.func, views.logoutUser)

    def test_func_autentikasi(self):
        found = resolve('/autentikasi/')
        self.assertEqual(found.func, views.autentikasi)

    def test_kelengkapan_formulir_di_login(self):
        response = Client().get('/autentikasi/login/')
        isi_html_kembalian = response.content.decode('utf8')  
        self.assertIn("LOGIN", isi_html_kembalian)
        self.assertIn("Enter username or email", isi_html_kembalian)
        self.assertIn("Enter password", isi_html_kembalian)

    def test_kelengkapan_formulir_di_register(self):
        response = Client().get('/autentikasi/register/')
        isi_html_kembalian = response.content.decode('utf8')  
        self.assertIn("REGISTER", isi_html_kembalian)
        self.assertIn("Username..", isi_html_kembalian)
        self.assertIn("First name..", isi_html_kembalian)
        self.assertIn("Last name..", isi_html_kembalian)
        self.assertIn("Email..", isi_html_kembalian)
        self.assertIn("Enter password..", isi_html_kembalian)
        self.assertIn("Re-enter Password..", isi_html_kembalian)

    def test_create_account(self):
        User.objects.create(username="kepepet", first_name="tim", last_name="kepepet", email="timkepepet@gmail.com", password="pergitim")
        self.assertEqual(User.objects.all().count(), 1)

class TestApp(TestCase):
	def test_apps(self):
		self.assertEqual(AutentikasiConfig.name, 'autentikasi')
		self.assertEqual(apps.get_app_config('autentikasi').name, 'autentikasi')

        


