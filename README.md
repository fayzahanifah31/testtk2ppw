#TK2

[![pipeline status][pipeline-badge]][commits-gl]
[![coverage report][coverage-badge]][commits-gl]

Anggota Kelompok A15 :
- 1906398995	Agung Mubarak
- 1906399726	Fayza Hanifah
- 1906399663	Kornelius Tarigan
- 1906398931	Afnadiati Hartatika
- 1906399764	Alkhadrina Rasyidah Azzah Zahra

Berikut link Heroku dari website Kepepetpergi : https://kepepet-pergi.herokuapp.com/

Latar Belakang :
Platform ini dibentuk berdasarkan hasil diskusi kami yang melihat minimnya akses informasi publik mengenai prosedur protokol kesehatan untuk mereka yang memiliki kepentingan darurat sehingga harus bepergian ke daerah yang lain. Melalui platform ini kami berharap mampu mengambil bagian dengan membantu memberikan informasi yang akurat dari sumber terpercaya untuk mengatasi persebaran virus COVID-19 di Indonesia.

Fitur :
- Mencari informasi prosedur protokol kesehatan di suatu daerah/provinsi
- Menyediakan kolom saran data daerah/provinsi baru atau yang belum tersedia
- Menyediakan form contact us untuk bertanya dan memberi saran untuk website
- Menampilkan rekapan status ketersediaan data pada setiap daerah/provinsi, dapat ditambah fitur filter penyajian sesuai dengan pulau
- Menyediakan berita-berita terkait COVID-19

[pipeline-badge]: https://gitlab.com/alkhadrina.rasyidah/tk2-ppw-a15/badges/master/pipeline.svg
[commits-gl]: https://gitlab.com/alkhadrina.rasyidah/tk2-ppw-a15/-/commits/master
[coverage-badge]: https://gitlab.com/alkhadrina.rasyidah/tk2-ppw-a15/badges/master/coverage.svg
