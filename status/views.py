from django.shortcuts import render
from django.http import HttpResponseRedirect
from status.forms import PulauForm
from main.models import *
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from django.contrib.auth.decorators import login_required
# Create your views here.


def home(request):
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = PulauForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required
            # ...
            # redirect to a new URL:
            return HttpResponseRedirect('{pulau}/'.format(pulau=request.POST['pulau_form']))

    # if a GET (or any other method) we'll create a blank form
    else:
        form = PulauForm()
    return render(request, 'status/home.html',{'form': form})


def detail(request,pulau):
    ver = VerifiedInfo.objects.all()
    l =[]
    for info in ver:
        if info.info_id.nama_provinsi.nama_pulau.nama==pulau:
            l.append(info.info_id.nama_provinsi)
    
    nama_pulau=pulau
    #ver2 = NonVerifiedInfo.objects.all()
    verp = Provinsi.objects.filter(nama_pulau=pulau)
    l2 =[]
    for infop in verp:
        if infop in l:
            continue
        else:
            l2.append(infop)
    # for info in ver:
    #     if info.info_id.nama_provinsi.nama_pulau.nama==pulau:
    #         l2.append(info.info_id.nama_provinsi)
    
    return render(request, 'status/detail.html',{'lst': l,'lst2':l2,'pulau':nama_pulau})

@login_required(login_url='autentikasi:login')
def detail_prov(request,pulau,prov):
    nama = prov.replace('%20', '')

    obj_prov = Provinsi.objects.get(nama=nama)
    obj_info = Info.objects.get(nama_provinsi=obj_prov)
    return render(request, 'status/detail_prov.html',{'prov':obj_prov,'info':obj_info})