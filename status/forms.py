from django import forms
from main.models import Pulau,Provinsi


pulau_list = Pulau.objects.values_list('nama')
PULAU_CHOICES = [('Jawa','Jawa'),('Sumatera','Sumatera'),('Kalimantan','Kalimantan'),('Sulawesi','Sulawesi'),('Papua','Papua')]



class PulauForm(forms.Form):
    pulau_form= forms.CharField(label='', widget=forms.Select(choices=PULAU_CHOICES))
