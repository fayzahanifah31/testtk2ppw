# Generated by Django 3.1.3 on 2020-11-17 08:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0004_auto_20201116_1205'),
    ]

    operations = [
        migrations.AlterField(
            model_name='saran',
            name='jenis_saran',
            field=models.CharField(choices=[('Perbaikan website', 'Perbaikan website'), ('Masukkan data provinsi', 'Masukkan data provinsi')], max_length=25),
        ),
    ]
