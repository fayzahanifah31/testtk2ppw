from django.contrib import admin
from .models import *

# Register your models here.
admin.site.register(Pulau)
admin.site.register(Provinsi)
admin.site.register(Info)
admin.site.register(WebsitePemda)
admin.site.register(TelponPemda)
admin.site.register(EmailPemda)
admin.site.register(VerifiedInfo)
admin.site.register(NonVerifiedInfo)
class AdminA(admin.ModelAdmin): 
    readonly_fields = ['tanggal']

admin.site.register(Saran, AdminA)

