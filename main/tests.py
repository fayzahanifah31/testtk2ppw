from django.test import LiveServerTestCase, TestCase, tag
from django.test import Client
from django.urls import reverse
from selenium import webdriver

from .models import *
from datetime import datetime

@tag('functional')
class FunctionalTestCase(LiveServerTestCase):
    """Base class for functional test cases with selenium."""

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        # Change to another webdriver if desired (and update CI accordingly).
        options = webdriver.chrome.options.Options()
        # These options are needed for CI with Chromium.
        options.headless = True  # Disable GUI.
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        cls.selenium = webdriver.Chrome(options=options)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()


class MainTestCase(TestCase):

    # URL test
    def test_root_url_status_200(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
        # You can also use path names instead of explicit paths.
        response = self.client.get(reverse('main:home'))
        self.assertEqual(response.status_code, 200)

    # Model test
    def test_model_pulau(self):
        new_pulau = Pulau.objects.create(nama='jawa')

        counting_all_available_model = Pulau.objects.all().count()
        self.assertEqual(counting_all_available_model, 1)

    def test_model_provinsi(self):
        new_pulau = Pulau.objects.create(nama='jawa')
        new_provinsi = Provinsi.objects.create(nama='Jawa Barat', nama_pulau=new_pulau)

        counting_all_available_model = Provinsi.objects.all().count()
        self.assertEqual(counting_all_available_model, 1)

    def test_model_info(self):
        new_pulau = Pulau.objects.create(nama='jawa')
        new_provinsi = Provinsi.objects.create(nama='Jawa Barat', nama_pulau=new_pulau)
        new_info = Info.objects.create(tanggal=datetime.today(), nama_provinsi=new_provinsi,protokol_kesehatan='')

        counting_all_available_model = Info.objects.all().count()
        self.assertEqual(counting_all_available_model, 1)

    def test_model_website_pemda(self):
        new_pulau = Pulau.objects.create(nama='jawa')
        new_provinsi = Provinsi.objects.create(nama='Jawa Barat', nama_pulau=new_pulau)
        new_info = Info.objects.create(tanggal=datetime.today(), nama_provinsi=new_provinsi,protokol_kesehatan='')
        new_website_pemda = WebsitePemda.objects.create(info_id=new_info, alamat='www.google.com')

        counting_all_available_model = WebsitePemda.objects.all().count()
        self.assertEqual(counting_all_available_model, 1)

    def test_model_email_pemda(self):
        new_pulau = Pulau.objects.create(nama='jawa')
        new_provinsi = Provinsi.objects.create(nama='Jawa Barat', nama_pulau=new_pulau)
        new_info = Info.objects.create(tanggal=datetime.today(), nama_provinsi=new_provinsi,protokol_kesehatan='')
        new_email_pemda = EmailPemda.objects.create(info_id=new_info, email='123@gmail.com')

        counting_all_available_model = EmailPemda.objects.all().count()
        self.assertEqual(counting_all_available_model, 1)

    def test_model_telpon_pemda(self):
        new_pulau = Pulau.objects.create(nama='jawa')
        new_provinsi = Provinsi.objects.create(nama='Jawa Barat', nama_pulau=new_pulau)
        new_info = Info.objects.create(tanggal=datetime.today(), nama_provinsi=new_provinsi,protokol_kesehatan='')
        new_telpon_pemda = TelponPemda.objects.create(info_id=new_info, nomor_telpon='088188828883')

        counting_all_available_model = TelponPemda.objects.all().count()
        self.assertEqual(counting_all_available_model, 1)

    def test_model_verified_info(self):
        new_pulau = Pulau.objects.create(nama='jawa')
        new_provinsi = Provinsi.objects.create(nama='Jawa Barat', nama_pulau=new_pulau)
        new_info = Info.objects.create(tanggal=datetime.today(), nama_provinsi=new_provinsi,protokol_kesehatan='')
        new_verified_info = VerifiedInfo.objects.create(info_id=new_info)

        counting_all_available_model = VerifiedInfo.objects.all().count()
        self.assertEqual(counting_all_available_model, 1)

    def test_model_non_verified_info(self):
        new_pulau = Pulau.objects.create(nama='jawa')
        new_provinsi = Provinsi.objects.create(nama='Jawa Barat', nama_pulau=new_pulau)
        new_info = Info.objects.create(tanggal=datetime.today(), nama_provinsi=new_provinsi,protokol_kesehatan='')
        new_non_verified_info = NonVerifiedInfo.objects.create(info_id=new_info, email='123@gmail.com')

        counting_all_available_model = NonVerifiedInfo.objects.all().count()
        self.assertEqual(counting_all_available_model, 1)

    def test_model_saran(self):
        new_saran = Saran.objects.create(tanggal=datetime.today(), email='123@gmail.com', jenis_saran='P', saran='')

        counting_all_available_model = Saran.objects.all().count()
        self.assertEqual(counting_all_available_model, 1)

class MainFunctionalTestCase(FunctionalTestCase):
    def test_root_url_exists(self):
        self.selenium.get(f'{self.live_server_url}/')
        html = self.selenium.find_element_by_tag_name('html')
        self.assertNotIn('not found', html.text.lower())
        self.assertNotIn('error', html.text.lower())