from main.models import Saran
from django.shortcuts import render,redirect
from django.http import JsonResponse
from django.contrib.auth.models import User


from .forms import PostForm

from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from django.contrib.auth.decorators import login_required

# Create your views here.

# def create(request):
#     posts = Saran.objects.all()
#     post_form = PostForm(request.POST or None)

#     if request.method == 'POST' :#post request dari browser
#         if post_form.is_valid():
#             post_form.save()
#             post_form = PostForm()
#             return redirect('formSaran:create')
#     context = {
#         'page_title':'Saran',
#         'post_form': post_form,
#         'posts':posts,
#     }
#     return JsonResponse(context)

def create(request):
	json = {'is_posted' : False}

	if(request.method == 'POST'):
		form = PostForm(data=request.POST)
		if(form.is_valid()):
			form.save()
			json['is_posted'] = True

	return JsonResponse(json)


