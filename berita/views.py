from django.shortcuts import render
from newsapi.newsapi_client import NewsApiClient
from .forms import ArticleForm
from django.shortcuts import redirect

from .models import *

class Berita:
	def __init__(self, judul, isi, gambar, website, alamat):
		self.judul = judul
		self.isi = isi
		self.gambar = gambar
		self.website = website
		self.alamat = alamat

# Create your views here.
def home(request):
	beritas = []
	try:
		newsapi = NewsApiClient(api_key='6fbbdcf18b614bccb89f053575af762b')
		news = newsapi.get_top_headlines(q='covid', country='id')
		for item in news['articles']:
			judul = item['title']
			isi = item['description']
			gambar = item['urlToImage']
			website = item['source']['name']
			alamat = item['url']
			berita = Berita(judul, isi, gambar, website, alamat)
			beritas.append(berita)
	except:
		pass
	
	articles = Article.objects.all()

	response = {
		'beritas' : beritas,
		'articles' : articles
		}
	return render(request, 'berita/home.html',response)

def articleForm(request):
	if(request.method == 'POST'):
		form = ArticleForm(request.POST)
		if(form.is_valid()):
			form.save()
			return redirect('/berita')
	else:
		form = ArticleForm(initial={'user' : request.user})

	response = {
		'input_form' : form
	}

	return render(request, 'berita/articleForm.html', response)

def articleDetail(request, article_id):
	article = Article.objects.get(article_id=article_id)

	response = {
		'article' : article
	}

	return render(request, 'berita/detail.html', response)

def editArticle(request, article_id):
	article = Article.objects.get(article_id=article_id)
	if(request.method == 'POST'):
		form = ArticleForm(request.POST, instance=article)
		if(form.is_valid()):
			form.save()
			return redirect('/berita')
	else:
		form = ArticleForm(instance=article)

	response = {
		'input_form' : form
	}

	return render(request, 'berita/articleForm.html', response)

def deleteArticle(request, article_id):
	article = Article.objects.get(article_id=article_id)
	article.delete()

	return redirect('/berita')