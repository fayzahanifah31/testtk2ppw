from django.urls import path

from .views import *

app_name = 'berita'

urlpatterns = [
    path('', home, name='home'),
    path('create/', articleForm, name='create'),
    path('detail/<int:article_id>/', articleDetail, name='detail'),
    path('edit/<int:article_id>/', editArticle, name='edit'),
    path('delete/<int:article_id>', deleteArticle, name='delete')
]