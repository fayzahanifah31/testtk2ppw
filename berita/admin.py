from django.contrib import admin
from .models import *

# Register your models here.
class AdminA(admin.ModelAdmin): 
    readonly_fields = ['date']

admin.site.register(Article, AdminA)