from django import forms
from tinymce.widgets import TinyMCE
from .models import *

class ArticleForm(forms.ModelForm):
    class Meta:
        model = Article
        fields = [
            'user',
            'title',
            'post'
        ]
        widgets = {
            'user' : forms.HiddenInput()
        }