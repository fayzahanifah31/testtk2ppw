from django.test import TestCase, Client
from django.urls import resolve
from django.apps import apps
from . import views, forms
from main.models import *
from .apps import PencarianConfig
from .views import pencarian
from .forms import PencarianForm
from datetime import datetime, date
import http
from http import HTTPStatus
# from django.contrib.auth import User, login, authenticate, logout

# Create your tests here.
class PencarianTest(TestCase):
    # test urls
    def test_url_get_pencarian(self):
        response = Client().get('/pencarian/')
        self.assertEqual(response.status_code, 200)

    # test views
    def test_func_pencarian(self):
        found = resolve('/pencarian/')
        self.assertEqual(found.func, views.pencarian)

    def test_templates_pencarian(self):
        response = Client().get("/pencarian/")
        self.assertTemplateUsed(response, 'pencarian/pencarian.html')

    def test_text_pencarian(self):
        response = Client().get("/pencarian/")
        html_response = response.content.decode('utf8')
        self.assertIn("Pencarian", html_response)
        self.assertIn("Nama Provinsi", html_response)
        self.assertIn("Search", html_response)
        self.assertIn("Masukkan nama provinsi", html_response)

class TestApp(TestCase):
	def test_apps(self):
		self.assertEqual(PencarianConfig.name, 'pencarian')
		self.assertEqual(apps.get_app_config('pencarian').name, 'pencarian')