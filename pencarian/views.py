from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from main.models import *
from .forms import PencarianForm
from pencarian import forms
from main import models
from django.contrib import messages

# Create your views here.

def pencarian(request):
    data_verified = VerifiedInfo.objects.all()
    form = PencarianForm(request.GET)
    context = {
        'form': PencarianForm(),
        'data' : data_verified,
    }

    if request.method == 'POST':
        found = False
        query = str(request.POST['nama'])
        for info in data_verified:
            if query.lower() in str(info.info_id.nama_provinsi).lower():
                context['info'] = info.info_id
                messages.success(request, ("Data '" + query + "' berhasil ditemukan! ✅"))
                found = True
                return render(request, 'pencarian/pencarian.html', context)
            
        if found == False:   
            messages.warning(request, "❌ OOPS ❌")
            messages.warning(request, (" Data '" + query + "' belum tersedia atau terdapat kesalahan ejaan nama provinsi. Mohon diperiksa kembali 🙏🏻"))
            return render(request, 'pencarian/pencarian.html', context)
                
    return render(request, 'pencarian/pencarian.html', context)