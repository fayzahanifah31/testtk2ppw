from django.urls import path

from . import views

urlpatterns = [
    path('', views.pencarian, name='pencarian'),
]